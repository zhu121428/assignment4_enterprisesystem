﻿using Assignment4.Persistence;
using Ninject.Modules;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Assignment4
{
    public class DomainFactoryModule : NinjectModule
    {
        public override void Load()
        {

            Bind<IInvoiceSystemUserRepo>().To<DatabaseInvoiceSystemUserRepository>();
            Bind<IInvoiceSystemInvoiceRepo>().To<DatabaseInvoiceSystemInvoiceRepository>();

        }
    }
}