﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Assignment4.Models
{
    public class Invoice
    {
        [Required]
        public int InvoiceId { get; set; }
        [Required(ErrorMessage = "Please provide a client name")]
        public string ClientName { get; set; }
        [Required(ErrorMessage = "Please provide a client address")]
        public string ClientAddress { get; set; }
        [Required(ErrorMessage = "Please provide date of shipment")]
        public DateTime DateofShipment { get; set; }
        [Required(ErrorMessage = "Please provide Payment due day")]
        public DateTime PaymentDueDay { get; set; }
        [Required(ErrorMessage = "Please provide product name")]
        public string ProductName { get; set; }
        [Required(ErrorMessage = "Please provide the quantity")]
        public int ProductQuantity { get; set; }
        [Required(ErrorMessage = "Please provide a price")]
        public decimal UnitPrice { get; set; }
        [Required(ErrorMessage = "Please provide a currency")]
        public string Currency { get; set; }
        [Required(ErrorMessage = "Please provide select ")]
        public bool? Paid { get; set; }

        public void Change(Invoice invoice)
        {
            //this.Name = product.Name;
            //this.Description = product.Description;
            //this.Price = product.Price;
            //this.Category = product.Category;
            this.ClientName = invoice.ClientName;
            this.ClientAddress = invoice.ClientAddress;
            this.DateofShipment = invoice.DateofShipment;
            this.PaymentDueDay = invoice.PaymentDueDay;
            this.ProductName = invoice.ProductName;
            this.ProductQuantity = invoice.ProductQuantity;
            this.UnitPrice = invoice.UnitPrice;
            this.Currency = invoice.Currency;
            this.Paid = invoice.Paid;


    }

    }
    

}