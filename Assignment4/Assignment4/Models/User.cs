﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;


namespace Assignment4.Models
{

    public class User
    {
        [Required]
        public int UserID { get; set; }
        [Required(ErrorMessage = "Please provide a UserName")]
        public string UserName { get; set; }
        [Required(ErrorMessage = "Please provide a Password")]
        public string Password { get; set; }
        [Required]
        public string Type { get; set; }
    }
}