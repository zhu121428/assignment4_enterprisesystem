﻿using Assignment4.Models;
using Assignment4.Persistence;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Assignment4.Controllers
{
    public class HomeController : Controller
    {
        private IInvoiceSystemUserRepo _invoiceSystemUserRepo;
        private IInvoiceSystemInvoiceRepo _invoiceSystemInvoiceRepo;

        public HomeController(IInvoiceSystemUserRepo invoiceSystemUserRepo,IInvoiceSystemInvoiceRepo invoiceSystemInvoiceRepo)
        {
            _invoiceSystemUserRepo = invoiceSystemUserRepo;
            _invoiceSystemInvoiceRepo = invoiceSystemInvoiceRepo;
            
        }
        // GET: Home
        [HttpGet]
        public ActionResult Index()
        {
            return View();
            //return View(_invoiceSystemInvoiceRepo.InvoiceList);
            //return View(_invoiceSystemUserRepo.UserList);
        }

        [HttpPost]
        public ActionResult Index(loginInRequirment information)
        {

            string username = information.Username;
            string password = information.Password;
            //_invoiceSystemUserRepo.UserList.Select(p => p.UserName == username && p.Password == password)
            User founduser = _invoiceSystemUserRepo.UserList.FirstOrDefault(p => p.UserName == username && p.Password == password);
            if (founduser != null)
            {
                if (founduser.Type.Equals("manager"))
                {
                    return RedirectToAction("ManagerPage");
                }
                return View("UserPage");

            }

            else
            {
                return View("Index");
            }
        }

        public ActionResult ManagerPage()
        {
            return View(_invoiceSystemInvoiceRepo.InvoiceList);
        }
        [HttpGet]
        public ActionResult UserPage()
        {
            return View();
        }
        [HttpPost]
        public ActionResult UserPage(Invoice invoice)
        {
            _invoiceSystemInvoiceRepo.SaveInvoice(invoice);
            return RedirectToAction("Thank");
        }
        [HttpGet]
        public ActionResult Edit(int id)
        {
            
            Invoice foundInvoice = _invoiceSystemInvoiceRepo.InvoiceList.FirstOrDefault(p => p.InvoiceId == id);

            
            if (foundInvoice != null)
            {
                
                return View(foundInvoice);
            }
            else
            {

                return RedirectToAction("Error");
            }
        }

        [HttpPost]
        public ActionResult Edit(Invoice invoice)
        {
            
            
            _invoiceSystemInvoiceRepo.SaveInvoice(invoice);

            //redirect to the list of products
            return RedirectToAction("ManagerPage");
        }

        public ActionResult Thank()
        {
            return View();
        }

        public ActionResult Error()
        {
            return View();
        }

        //TODO: add action method to display the product details

        //TODO: add action methods to implement the delete operation for the product
    }
}
