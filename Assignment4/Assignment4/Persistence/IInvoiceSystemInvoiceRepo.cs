﻿using Assignment4.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Assignment4.Persistence
{
    public interface IInvoiceSystemInvoiceRepo
    {
        //IEnumerable<User> UserList { get; }
        IEnumerable<Invoice> InvoiceList { get; }
        void SaveInvoice(Invoice invoice);
    }
}