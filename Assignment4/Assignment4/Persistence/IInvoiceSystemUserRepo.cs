﻿using Assignment4.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Assignment4.Persistence
{
    public interface IInvoiceSystemUserRepo
    {
        IEnumerable<User> UserList { get; }
        //IEnumerable<Invoice> InvoiceList { get; }
    }
}
