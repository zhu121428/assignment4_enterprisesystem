﻿using Assignment4.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace Assignment4.Persistence
{
    class SystemDbInvoiceContext : DbContext
    {
        public SystemDbInvoiceContext() : base("InvoiceSystemDbConnection")
        {

        }
       // public DbSet<User> Users { get; set; }
        public DbSet<Invoice> Invoices { get; set; }

    }

}