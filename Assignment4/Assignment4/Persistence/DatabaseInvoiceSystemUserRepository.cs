﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Assignment4.Models;

namespace Assignment4.Persistence
{
    public class DatabaseInvoiceSystemUserRepository : IInvoiceSystemUserRepo
    {
        private SystemDbUserContext _SystemDbUserContext;

        public DatabaseInvoiceSystemUserRepository(){

            _SystemDbUserContext = new SystemDbUserContext();

            }

        //public IEnumerable<Invoice> InvoiceList
        //{
        //    get
        //    {
        //        return _SystemDbUserContext.Invoices;
        //    }
        //}

        public IEnumerable<User> UserList
        {
            get
            {
                return _SystemDbUserContext.Users;
                
            }
        }

        //public void CheckUser(User user)
        //{
        //    Application applicationEntity = _ApplicationDbContext.Applications
        //           .Find(application.ApplicationId);
        //    User userEntity = _SystemDbUserContext.Users.Find(user.)
        //}
    }
}