﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Assignment4.Models;

namespace Assignment4.Persistence
{
    public class DatabaseInvoiceSystemInvoiceRepository : IInvoiceSystemInvoiceRepo
    {
        private SystemDbInvoiceContext _SystemDbInvoiceContext;
        public DatabaseInvoiceSystemInvoiceRepository()
        {

            _SystemDbInvoiceContext = new SystemDbInvoiceContext();

        }
        public IEnumerable<Invoice> InvoiceList
        {
            get
            {
                return _SystemDbInvoiceContext.Invoices;
            }
        }
        

        public void SaveInvoice(Invoice invoice)
        {
            if(invoice.InvoiceId == 0)
            {
                _SystemDbInvoiceContext.Invoices.Add(invoice);
            }
            else
            {
                Invoice invoiceEntity = _SystemDbInvoiceContext.Invoices.Find(invoice.InvoiceId);
                if(invoiceEntity != null)
                {
                    invoiceEntity.Change(invoice);
                }
            }
            _SystemDbInvoiceContext.SaveChanges();
        }
    }
}